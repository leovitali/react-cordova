import React from 'react';
import { Router, Route } from 'react-router';

import Home from '../home/';
import { Cidade, Estado } from '../cadastro/'

class App extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
				<Router>
					<Route path="/" component={Home}/>
					<Route path="/cadastro">
						<Route path="estado" component={Estado}/>
						<Route path="/cadastro/cidade" component={Cidade}/>
					</Route>
				</Router>
			);
		}
	}

	export default App;
