import React from 'react';
import FieldGroup from '../../field-group/';
import { Button } from 'react-bootstrap'; 

class Cidade extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'Cidade';
    }
    render() {
        return (
        	<form>
				<FieldGroup
					id="description"
					label="Descrição"
					type='text'
				 	placeholder='Descrição'
				/>
				<FieldGroup
					id="description"
					label="Descrição"
					type='select'
				 	placeholder='Descrição'
				/>
				<Button type="submit" bsStyle='success'>
					Salvar
				</Button>
			</form>
        );
    }
}

export default Cidade;
