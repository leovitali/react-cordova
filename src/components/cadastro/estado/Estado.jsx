import React from 'react';
import { Button } from 'react-bootstrap';
import FieldGroup from '../../field-group/';

class Estado extends React.Component {

	constructor(props) {
		super(props);
		this.displayName = 'Cadastro de Estados';
	}

	render() {
		return (
			<form>
				<FieldGroup
					id="description"
					label="Descrição"
					type='text'
				 	placeholder='Descrição'
				/>
				<FieldGroup
					id="initials"
					label="Sigla"
					type='text'
				 	placeholder='Sigla'
				/>
				<FieldGroup
					id="country"
					label="Country"
					type='text'
				 	placeholder='País'
				/>
				<Button type="submit" bsStyle='success'>
					Salvar
				</Button>
			</form>
		);
	}
}

export default Estado;
