import React from 'react';
import { FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';

class FieldGroup extends React.Component {
    constructor(props) {
        super(props);
    }

    createFormControl(type, placeholder) {
    	if(type != 'select') {
    		return (
				<FormControl
					type={type}
					placeholder={placeholder}
				/>
			);
    	}

   		return (
   			<FormControl componentClass="select" >
        		<option value="select">Selecione um Estado</option>
        		<option value="other">PR</option>
        		<option value="other">SC</option>
        		<option value="other">RS</option>
      		</FormControl>
   		);
    }

    render() {
    	let { type, placeholder } = this.props;
        return (
			<FormGroup controlId={this.props.id}>
				<ControlLabel>{this.props.label}</ControlLabel>
				{this.createFormControl(type, placeholder)}
			</FormGroup>
		);
    }
}



export default FieldGroup;
