import React from 'react';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router';
import image from './images/hue.jpg';

class Home extends React.Component {
    
    constructor(props) {
        super(props);
        this.displayName = 'Home';
    }

    render() {
        return (
        	<div>
                <h1>Sinto muito, mas como bom brasileiro deixei para ultima hora.</h1>
        		<Link to="/cadastro/estado"><Button bsStyle='primary' bsSize="large" block>Cadastro de Estados</Button></Link>
        		<br/>
                <Link to="/cadastro/cidade"><Button bsStyle='primary' bsSize="large" block>Cadastro de Cidades</Button></Link>
        	    <div className="text-center">
                    <img src={image}/>
                </div>
            </div>
        	);
    }
}

export default Home;
